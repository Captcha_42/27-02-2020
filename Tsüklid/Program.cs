﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tsüklid
{
    class Program
    {
        static void Main(string[] args)
        {
            // tavaline for tsükkel
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Teen seda asja {i}-{(i == 1 || i == 2 ? "st" : "ndat")} korda");
            }

            // viis kuidas tsüklis jääda küsima kuni antakse vastus

            int vastus = 0;
            for (
                Console.WriteLine("Mis su palk on ?");
            int.TryParse(Console.ReadLine(), out vastus);
            Console.WriteLine("kirjuta ilusti.")
            )
            { }
            Console.WriteLine($"selge, su palk on {vastus}");

            //while tsükkel

            while (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
            //for(;DateTime.Now.DayOfWeek == DayOfWeek.Friday)
            {
                Console.WriteLine("ootan reedet");
                System.Threading.Thread.Sleep(1000);
            }

            //do tsükkel

            do
            {
                Console.WriteLine("ootan reedet");
                System.Threading.Thread.Sleep(1000);
            } while (DateTime.Now.DayOfWeek != DayOfWeek.Thursday);
        }

    }

}
  