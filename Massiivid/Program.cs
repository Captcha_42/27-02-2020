﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {
            int i1 = 0;
            int i2 = 7;
                
            int[] arvud;                                    //tegemist on massiiviga. Sellel pole hetkel sisu. 
            arvud = new int[i2];                            //nüüd on massiivil sisu - ta koosneb 10-st int'ist

            for(int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = i * i;                           // pöördumine indeksi kaudu i * i ; i1 * i1; i2 * i2; ...
            }

            foreach (var x in arvud) Console.WriteLine(x);

            int[] teisedArvud = new int[10];                // muutja defineerimine KOOS väärtusega
            int[] kolmandadArvud = teisedArvud;
            kolmandadArvud[3] = 7;
            Console.WriteLine(teisedArvud[3]);

            int[] veelYks = new int[5] { 1, 3, 6, 2, 7]};         // massiiv ja initializer > arvud või avaldised
            veelYks = new int[] { 1, 3, 6, 2, 7, 6, 3, 2, 1 ]};   // Kui int [*] ei täida, siis loendatakse automaatselt

            int[][] paljuarve;                                    // massiiv võib koosneda teistest massiividest - massiivide massiiv
            paljuarve = new int[3][];
            paljuarve = new int[] { 1, 2, 3, 4 };                 // jrk 0 massiiv
            paljuarve = new int[] { 7, 8, 9 };                    // jrk 1 massiiv
            paljuarve = new int[] { 5, 6};                        // jrk 2 massiiv
            Console.WriteLine(paljuarve[1][2]); 

            int[,] tabel = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 0, 0, 10 } };
            Console.WriteLine(tabel[2, 2]);

            string[] kuud =                                       // massiivi võib kokku panna suvalistest asjadest
             {
               "Jaanuar",
               "Veebruar",
               "Märts"
             };
           



        }
    }
}
