﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teise_paeva_konsool
{
    class Program
    {
        static void Main(string[] args)

        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            // tingimuslik lause, siia ei panda lõppu ; - tekib empty statement, mida indikeerib roheline laineline joon ; all.
            {
                Console.WriteLine("täna lähme sauna");
                int vihtadeArv = 7;
                Console.WriteLine($"võtame kaasa {vihtadeArv} vihta");
                // muutja, mis on defineeritud ploki sees ei ole kasutatav sellest väljapool.
                // muutuja, mis on ploki hierarhias kõrgemal on kasutatav ka alamplokis.
                // if tingimuse koosneb lausetest alates 0.... lauseplokk eraldatakse {} sulud võib ära jätta, kui plokk koosneb ühest lausest
                // nt. if (vihtadeArv == 0) Console.WriteLine("täna ei vihtle");
            }
            else
            {

            }
                //need laused, siin plokis täidetakse, kui if-i tagune asi on false. 

                if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                {
                    // laupäevased laused
                }
                else if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                {
                    // tingimus
                }
                else
                {
                    //tingimus
                }

            // muutja skoop
            string see = "see on protseduuri tasemel muutuja";
            {
                int kohalik = 5;
            }

            {
                string kohalik = "kaheksa";
            }
            
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    // laused, mida täidetakse laupäeval
                    Console.WriteLine("täna mängime jalkat");
                    //break;
                    goto case DayOfWeek.Sunday;
                case DayOfWeek.Sunday:
                    // laused, mida täidetakse pühapäeval
                    Console.WriteLine("täna joome õlut");
                    break;

                case DayOfWeek.Thursday:
                     // laused, mida täidetakse kolmapäeval
                     Console.WriteLine("lähen mängin kossu");
                    goto default;
                default:
                    //laused, mida täiedetakse muudel päevadel
                    Console.WriteLine("lähen tööle");
                    break;
            }
            Console.Write("Press Any Key");
            Console.ReadLine();
            }
          }
}
