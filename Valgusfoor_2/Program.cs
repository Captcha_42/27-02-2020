﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valgusfoor_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string tuli;

            do
            {
                Console.WriteLine("Mis tuli valgusfooris põleb ?");

                tuli = Console.ReadLine().ToLower();

                if (tuli == "punane" | tuli == "red")
                {
                    Console.WriteLine("Jää seisma");
                }
                else if (tuli == "kollane" | tuli == "yellow")
                {
                    Console.WriteLine("Aeglusta seisuni");
                }
                else
                    Console.WriteLine("Võid läbi sõita");

            } while (tuli != "roheline" && tuli != "green");


            bool oota = true;
            do
            {
                Console.WriteLine("Mis tuli valgusfooris põleb ?");

                tuli = Console.ReadLine().ToLower();

                if (tuli == "punane" | tuli == "red")
                {
                    Console.WriteLine("Peatu!");
                    oota = false;
                }
                else if (tuli == "kollane" | tuli == "yellow")
                {
                    Console.WriteLine("Aeglusta seisuni");
                    oota = false;
                }
                else                 
                {
                    Console.WriteLine("Võid läbi sõita");
                    oota = true;
                }                
            } while (oota != true);
        }
    }
    }
